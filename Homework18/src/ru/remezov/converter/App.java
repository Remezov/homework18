package ru.remezov.converter;

import java.io.*;

public class App {
    public static void main(String[] args) throws IOException {
        String str = "Привет, мир!\nКак дела?\nЧто делаешь?";
        saveToFile("utf-8.txt", str, "UTF8");
        String readStr = readFromFile("utf-8.txt", "UTF8");
        System.out.println(readStr);
        saveToFile("koi8.txt", readStr, "koi8");
    }

    private static void saveToFile(String outFile, String str, String charsetName) throws IOException {
        try(OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(outFile), charsetName)) {
            osw.write(str);
        }
    }

    private static String readFromFile(String filename, String encoding) throws IOException {
        String result = "";
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(filename), encoding))){
            String line = null;
            while ((line = reader.readLine()) != null) {
                result += line+"\n";
            }
            return result;
        }
    }
}
