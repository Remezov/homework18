package ru.remezov.library;

import java.io.Serializable;

public class Book implements Serializable {
    private String name;
    private int publicationDate;
    private String author;

    public Book(String name, int publicationDate, String author) {
        this.name = name;
        this.publicationDate = publicationDate;
        this.author = author;
    }

    public String toString() {
        return String.format("%s, %d, %s", name, publicationDate, author);
    }
}
